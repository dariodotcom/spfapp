/* 
 * Copyright 2014 Jacopo Aliprandi, Dario Archetti
 * 
 * This file is part of SPF.
 * 
 * SPF is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free 
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * SPF is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with SPF.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
package it.polimi.spf.framework.security;

import java.nio.charset.Charset;
import java.security.GeneralSecurityException;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import android.text.TextUtils;
import android.util.Base64;

public class TokenCipher {

	public static class WrongPassphraseException extends Exception {

		private static final long serialVersionUID = 1L;

	}

	private static final String PREFIX = "token:";
	private static final Charset mCharset = Charset.forName("US-ASCII");

	public static String encryptToken(String token, String passphrase) throws GeneralSecurityException {
		byte[] passBytes = passphrase.getBytes(mCharset);
		byte[] tokenBytes = (PREFIX + token).getBytes(mCharset);
		SecretKeySpec spec = new SecretKeySpec(passBytes, "AES");
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, spec, new IvParameterSpec(new byte[16]));
		byte[] encrypted = cipher.doFinal(tokenBytes);
		return Base64.encodeToString(encrypted, Base64.DEFAULT);
	}

	public static String decryptToken(String cryptedToken, String passphrase) throws GeneralSecurityException, WrongPassphraseException {
		Charset charset = Charset.forName("US-ASCII");
		byte[] passBytes = passphrase.getBytes(charset);
		byte[] cryptedBytes = Base64.decode(cryptedToken, Base64.DEFAULT);
		SecretKeySpec spec = new SecretKeySpec(passBytes, "AES");
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.DECRYPT_MODE, spec, new IvParameterSpec(new byte[16]));
		byte[] decrypted = cipher.doFinal(cryptedBytes);
		String prefixedToken = new String(decrypted, mCharset);
		
		String[] split = TextUtils.split(prefixedToken, ":");
		if(split.length < 2 || !split[0].equals("token")){
			throw new WrongPassphraseException();
		}
		
		return split[1];
	}

}
