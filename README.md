# SPF: Social Proximity Framework #

## What is SPF ##
Social Proximity Framework (SPF) enables social interactions in a social smart spaces. It provides an additional software layer on top of a networking middleware to offer different functions enabling social interaction in proximity. External applications may leverage SPF to advertise a social profile in proximity or to discover other profiles matching different queries associated to social interests. Moreover, SPF offers a software library that can be used to access the framework and implement applicative services, without dealing with the complexities of networking programming. SPF is currently conceived and implemented for Android devices. SPF Library can be found here: https://bitbucket.org/dariodotcom/spflib/ 

## SPF Features ##
### SPF Profile  ###
A shared profile that can be personalized by the user and by applications. 
### SPF Search ###
Allows applications to search remote users according to queries over the user profile. With this function applications may find people that matches specific characteristics (e.g. interests, skills, ...) 
### SPF Service Registration ###
Allows applications to register services, that can be used to implement a simple chat, exchange contents or even more complex interactions. SPF will take care of all the networking operation needed to let the service work. 
### SPF Advertising  ###
Allows the user to advertise selected fields of his/her profile. Applications may define triggers over the advertised profiles. A trigger is composed by a query and an action. Queries are the same of the search and allow to select people according to their user profile contents; actions can be of two types and allow to be notified when a person is in proximity or to send simple messages to the remote user.

SPF is a project developed at 
Dipartimento di Elettronica Informazione e Bioingegneria
Politecnico di Milano, Milan, Italy